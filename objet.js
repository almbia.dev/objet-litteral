const movie = {
    name: "Whiplash",
    releaseDate: "2014",
    director: "Damien Chazelle",
    actors: ["Miles Teller", "JK Simmons"],
    favoriteMovie: true, // Changed to a boolean
    rating: 9.8,
    duration: 0,
    setDuration: function(hours, minutes) { // Define a function to set duration
        this.duration = hours * 60 + minutes; // Calculate duration in minutes
    }
};

movie.setDuration(1, 47);
console.log(movie.duration); // Output: 107
